VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Sheet3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
Sub finish()
    Worksheets("Records").Activate
End Sub
Sub a_it()
Call pinfo
    Dim putit, raw, tmplib, tmplib2 As Variant, code, room, types As String, sl As Integer, stime, etime, day, weeks, roomnum
    code = "-"
    room = "-"
    weeks = "-"
    etime = "-"
    stime = "-"
    day = "-"
    types = "-"
    'putit() = Split(Sheet3.Range("B3"), Chr(13))
    putit = Split([B3], Chr(10))
    sl = UBound(putit)
    If sl < 2 Then
        [B3] = "Content Error"
        Exit Sub
    End If
    code = Left(putit(0), 6)
    raw = Split(putit(0), " ")
    i = 0
    k = 0
'Course type
    For i = 0 To UBound(raw)
        If InStr(1, raw(i), "Lecture") > 0 Then
            types = "Lecture"
            'k = i
            Exit For
        ElseIf InStr(1, raw(i), "Seminar") > 0 Then
            types = "Seminar"
            'k = i
            Exit For
        Else: types = "???"
        End If
    Next
    
'Course Day
    For i = 0 To UBound(raw)
        If InStr(1, raw(i), "Monday") > 0 Then
            day = "Monday"
            k = i
            Exit For
        ElseIf InStr(1, raw(i), "Tuesday") > 0 Then
            day = "Tuesday"
            k = i
            Exit For
        ElseIf InStr(1, raw(i), "Wednesday") > 0 Then
            day = "Wednesday"
            k = i
            Exit For
        ElseIf InStr(1, raw(i), "Thursday") > 0 Then
            day = "Thursday"
            k = i
            Exit For
        ElseIf InStr(1, raw(i), "Friday") > 0 Then
            day = "Friday"
            k = i
            Exit For
        Else: day = "???"
        End If
    Next
    
    stime = raw(k + 1)
    etime = raw(k + 3)
    tmplib = Split(putit(0), "Weeks: ")
    weeks = Replace(tmplib(1), " ", "")
    
    tmplib2 = Split(putit(2), "Location: ")
    t = Trim(types & " ")
    If tmplib2(1) = "Auditorium" Then
        room = "Audi"
        ElseIf InStr(1, tmplib2(1), t) > 0 Then
            tmplib3 = Split(tmplib2(1), "- Level")
            roomnum = Replace(Right(tmplib3(0), 4), " ", "")
            l = InStr(1, tmplib2(1), t)
            building = Left(tmplib2(1), l - 2)
            Select Case building
             Case Is = "Teaching Building"
                bn = "TB"
             Case Is = "Student Service Building"
                bn = "SSB"
             Case Else
                bn = building
            End Select
        room = bn & " " & roomnum
    End If
    [C11] = code
    [F11] = day
    [I12] = room
    [I11] = weeks
    [F12] = stime
    [F13] = etime
    [C13] = types
    mname = [c12]
    [B15] = code & " " & types & " Successful"
'add to database
'By William ZHOU
Call addarry(day, stime, etime, mname, room, weeks, types, code)
Call reapply
End Sub
Sub clearinfo()
    [B3] = ""
End Sub
