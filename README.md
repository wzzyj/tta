This system may CANNOT 100% accurately analyse the data.
You are using this system with your own risk.

======================================================
Automatic UNNC (Course Planner™) Timetable information Analyse System
Last update 16/02/2013
Author William ZHOU
Version 1.01
Bug report: https://github.com/wzzyj/tta/issues
Download: http://blog.wkj8.com/20130215-696.html
======================================================

Features
1. Automatically analyse input information
2. Rewrite & structure information into user-friendly mode
3. Print timetable in one page.
4. Since it translate natural language into formated data, you can easily write a programme to:
Export it to formatted CSV and import to Calendar service like Google Calendar/iCal/Outlook
Easily re-structure it into personal Grid timetable.

Notice
1. This is a XLSM file. Please give this file Macros privilege to anaylse the data. (See this pic in the File).
2. Please follow the CC licenses to share/copy/edit this file.
转载请遵照既定CC许可协议[保留署名，非商业用途，相同方式共享]。谢绝抄袭。谢绝抄袭。谢绝抄袭。
3. Software Compatibility: Office 2007 – 2013, Recommand: Office 2013; Major Compatibility: IB/FAM/IET/ES/IC/IS, Y2-Y4
4. This system may CANNOT 100% accurately analyse the data. Always Double Check when you finish!!!

License
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/.

Read More: http://blog.wkj8.com/20130215-696.html
